var Webpack = require("webpack")
,   PROD    = process.env.PROD_DEV ? true: false
;

var plugins = [
  new Webpack.DefinePlugin({
    'process.env': {
      'isProduction': PROD
    }
  })
];

if (PROD) {
  plugins.push(
    new Webpack.optimize.UglifyJsPlugin()
  );
}

module.exports = {
  entry: "./src/app.js",
  output: {
    filename: "./public/js/bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /(node_modules|bower_components)/,
        query: {
          presets:['es2015', 'react']
        }
      }
    ]
  },
  plugins: plugins
}
