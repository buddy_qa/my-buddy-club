import React from 'react';
import HeaderView from '../../views/HeaderView/HeaderView';

class CoreLayout extends React.Component {
  render() {
    return (
      <div>
        <HeaderView />
        {this.props.children}
      </div>
    );
  }
}

export default CoreLayout;
