import { compose, createStore, applyMiddleware, combineReducers } from 'redux';
import categories from '../reducers/Categories';
import thunk from 'redux-thunk';

const middlewares = [thunk];

//adding logger for dev environment
if (!process.env.isProduction) {
  const createLogger = require('redux-logger');
  const logger = createLogger();
  middlewares.push(logger);
}

const reducer = combineReducers({
  categories,
});

const store = compose(
  applyMiddleware(...middlewares)
)(createStore)(reducer);

export default store;
