import React, { Component } from 'react';
import { getCategories } from '../../actions/Actions';
import { connect } from 'react-redux';

const mapStateToProps = (state) => ({
  categories: state.categories
});

class HeaderView extends React.Component {
  componentDidMount() {
    this.props.dispatch(getCategories());
  }

  render() {
    console.log(this.props)
    return (
      <header className="page">
        <div className="logo">
          <hr className="line-1" />
          <a href="#">My Buddy - Club</a>
          <span>Never be alone</span>
          <hr className="line-1" />
        </div>
        <div id='cssmenu' className="align-center">
          <ul>
             <li className="active"><a href='index.php'><span>Home</span></a></li>
              {
                this.props.categories.map((category, index) => {
                  return <li className='has-sub'>
                    <a href="/{category.nicename}">
                      <span>{category.name}</span>
                    </a>
                    <ul>
                      {
                        category.childrens.map((child, index) => {
                          return <li>
                            <a href="/{child.nicename}">
                              <span>{child.name}</span>
                            </a>
                          </li>
                        })
                      }
                    </ul>
                  </li>
                })
              }
            <li className='last'><a href='login.php'><span>Login</span></a></li>
          </ul>
        </div>
      </header>
    );
  }
}

export default connect(mapStateToProps)(HeaderView);
