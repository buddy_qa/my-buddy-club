export default function(sequelize) {
  return sequelize.define("Categories", {
    id: {
      type: 'SMALLINT(6)',
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: 'VARCHAR(100)',
      allowNull: false
    },
    nicename: {
      type: 'VARCHAR(100)',
      allowNull: false
    },
    description: {
      type: 'LONGTEXT',
      allowNull: false
    },
    parent: {
      type: 'TINYINT(1)',
      allowNull: false,
      defaultValue: '0'
    },
    visibility: {
      type: 'TINYINT(1)',
      allowNull: false,
      defaultValue: '0'
    } 
  },
  {
    tableName: 'categories',
    timestamps: false,
    underscored: true
  });
};
