import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

let basename = path.basename(module.filename)
,   db       = {}

export let sequelize = new Sequelize(
  'mybuddy',
  'root',
  '',
  {
    timezone: '+00:00',
  }
);

fs
  .readdirSync(__dirname)
  .filter((file) => {
    return (0 !== file.indexOf('.')) && (file !== basename) && ('.js' === file.slice(-3));
  })
  .forEach((file) => {
    db[file.slice(0, -3)] = sequelize['import'](path.join(__dirname, file));
  })
;

db.sequelize = sequelize;
db.Sequelize = Sequelize;
const { Categories } = sequelize.models;

export default db;
export { Categories };
