import { Categories } from '../models/Models';

function formatCategories(categories) {
  let parents = [];
  let childrens = [];

  categories.forEach((category) => {
    if (category.parent == 0) {
      category.childrens = [];
      parents.push(category);
    } else {
      childrens.push(category);   
    }
  });

  parents.forEach((parent) => {
    childrens.forEach((children) => {
      if (parent.id == children.parent) {
        parent.childrens.push(children);
      }
    })
  });

  return parents;
}

function getCategories(req, res, next) {
  Categories
    .findAll({ order: 'parent', raw: 'True' })
    .then((data) => res.json(formatCategories(data)))
    .catch(next)
  ;
}

export default {
  getCategories
};
