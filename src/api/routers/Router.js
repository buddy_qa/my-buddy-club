import { Router as ExpressRouter } from 'express';
import CategoryController from '../controllers/CategoryController';

const Router = ExpressRouter();

Router.route('/categories')
  .get(CategoryController.getCategories)
;

module.exports = Router;
