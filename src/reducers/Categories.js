import { RECEIVE_CATEGORIES } from '../constants/ActionTypes';

export default function(state = [], action) {
  switch (action.type) {
    case RECEIVE_CATEGORIES	:
      return action.data;
    default:
      return state;
  }
};
