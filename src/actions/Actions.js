import * as types from '../constants/ActionTypes';
import fetch from 'isomorphic-fetch';

function receiveCategories(categories) {
  return {
    type: types.RECEIVE_CATEGORIES,
    data: categories
  };
}

export function getCategories() {
  return dispatch => {
    return fetch('/api/categories', { 
      credentials: 'same-origin',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(req => req.json())
      .then(categories => {
        dispatch(receiveCategories(categories));
      });
  };
}