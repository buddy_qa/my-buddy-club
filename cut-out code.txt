INDEX.PHP

<!-----------------content-box-4-------------------->
				<section class="content-box boxstyle-2 box-4">
					<div class="page">
						<div class="row wrap-box"><!--Start Box-->
							<div class="header">
								<hr class="line-1">
								<h2 style="color: #fff;">Latest event:</h2>
							</div>
							<div class="post">
								<div class="col-1-2">
									<img src="images/event.jpg"/>
								</div>
								<div class="col-1-2">
									<div class="wrapper">
										<h3>The title on the article</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam viverra convallis auctor. Sed accumsan libero quis mi commodo et suscipit enim lacinia. Morbi rutrum vulputate est sed faucibus.</p>
										<a class="button" href="#">Read More</a>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</section>