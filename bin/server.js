require('babel-register');

let express      = require('express')
,   app          = express()
,   bodyParser   = require('body-parser')
,   env          = process.env.NODE_ENV || 'dev'
,   path         = require('path')
;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  console.log('%s %s', req.method, req.path);
  next();
});
app.use('/api/', require('../src/api/routers/Router'));
app.use(express.static('./public'));
app.get(/^\/[^\/]+$/, (req, res) => {
  res.sendFile(path.resolve('.../', 'public', 'index.html'));
});

const listener = app.listen(
  8000,
  () => {
    console.log("Server is Running on : -> Port: %s, with Environment: %s", listener.address().port, env);
  }
);